# Description: #
In this exercise you need to create a database in mongodb and a python program to insert json data into the database. You will also need to write a Jupyter notebook to validate your work. The provided json files contain annotation data for bank cheque images.

The goal of this exercise is to assess your:
- Database design skills.
- Python programing level.
- Implementation skills.
- Logical thinking.

![System diagram](images/python-mongodb.jpg)

### Part1 Database:
- Study database design in the provided json file and create all the necessary collections.
- Based on your discretion, create indexes that should be created on columns that are expected to be used extensively during the retrieval phase.
- Create the necessary references between collections.

###  Part2 Python application script: 
Create a python script that does the following:
- Load the json data from file and insert the records in their corresponding database collections. The application should assume multiple json files are fed from a source directory.
- When data is being inserted, the application should assure and properly handle duplicated data.
- The program should properly handle network disconnection cases.

### Part3 Validation: 
- For validation purposes, write a Jupyter notebook that shows a comparison between the records in the json files and in the database for each collection. 
- Pay attention to text data and make sure it is properly encoded in the database.

### Part4: Packaging: 
- Build a docker image that during the run it cleans the database, executes the python script first and then runs jupyter notebook.
- Database cleaning should be a configuration parameter.

### notes:
- A recommended practice is to create all database objects (in part1) through application code (in part2). So when the python script runs, it first assures that all database objects are created. Then it runs the migrating logic.
- In python code you need to rely on only one package to interact with mongodb. For example, you could use either "mongoengine" or "pymongo". 
- Upload your code on git repository.
- Database configuration (IP/Port/Database_name/User/Password) must be passed as configuration parameters to docker during the run.


